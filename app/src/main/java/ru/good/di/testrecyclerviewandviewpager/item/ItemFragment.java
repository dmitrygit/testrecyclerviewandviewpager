package ru.good.di.testrecyclerviewandviewpager.item;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

import ru.good.di.testrecyclerviewandviewpager.R;
import ru.good.di.testrecyclerviewandviewpager.model.Photo;
import ru.good.di.testrecyclerviewandviewpager.model.PhotoLab;

public class ItemFragment extends Fragment {
    static final String TAG = "VP_TWO";

    private static final String ARG_PHOTO_ID = "photo_id";

    private TextView mItemField;

    private Photo mPhoto;


    /**
     *    Более правильное решение — сохранение идентификатора в месте, принадлежащем ItemFragment
     * (вместо хранения его в личном пространстве ListActivity). В этом случае объект ItemFragment
     * может прочитать данные, не полагаясь  на  присутствие  конкретного  дополнения  в  интенте
     * активности. Такое«место», принадлежащее фрагменту, называется пакетом аргументов
     * (arguments bundle).
     *
     *    Присоединение должно быть выполнено после создания
     * фрагмента, но до его добавления в активность.
     * Для  этого  программисты  Android  используют  схему  с  добавлением  в  класс
     * Fragment статического метода с именем newInstance(). Этот метод создает экзем-
     * пляр фрагмента, упаковывает и задает его аргументы.
     */
    public static ItemFragment newInstance(UUID photoId){
        //Пакет аргументов (arguments bundle).
        Bundle args = new Bundle();

        /**
         *  добавление аргумента.
         * args.putSerializable(EXTRA_MY_OBJECT, myObject);
         * args.putInt(EXTRA_MY_INT, myInt);
         * args.putCharSequence(EXTRA_MY_STRING, myString);
         */
        args.putSerializable(ARG_PHOTO_ID, photoId);

        ItemFragment fragment = new ItemFragment();

        /**
         * Чтобы присоединить пакет аргументов к фрагменту, вызывается метод:
         */
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Когда фрагменту требуется получить доступ к его аргументам, он вызывает метод
         * getArguments() класса Fragment, а затем один из get-методов Bundle для конкретного типа.
         */
        UUID photoId = (UUID) getArguments()
                .getSerializable(ARG_PHOTO_ID);
        mPhoto = PhotoLab.get(getActivity()).getPhoto(photoId);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.item_fragment, container, false);

        //поле имя
        mItemField = (TextView)v.findViewById(R.id.item_name_text_view);
        mItemField.setText(mPhoto.getName() + "");


        return v;
    }

}
