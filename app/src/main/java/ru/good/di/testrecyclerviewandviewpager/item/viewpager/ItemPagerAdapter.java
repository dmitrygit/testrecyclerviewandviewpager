package ru.good.di.testrecyclerviewandviewpager.item.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import ru.good.di.testrecyclerviewandviewpager.item.ItemFragment;
import ru.good.di.testrecyclerviewandviewpager.model.Photo;

/**
 * Создаем PagerAdapter
 * Используя FragmentStatePagerAdapter — субкласс PagerAdapter, который берет на себя мно-
 * гие технические подробности
 */
public class ItemPagerAdapter extends FragmentStatePagerAdapter {

    private List<Photo> mListPhotos;


    public ItemPagerAdapter(FragmentManager fm, List<Photo> listPhotos) {
        super(fm);
        mListPhotos = listPhotos;
    }

    @Override
    public Fragment getItem(int position) {
        Photo photo = mListPhotos.get(position);

        /**
         * Когда активности-хосту потребуется экземпляр этого фрагмента, она вместо пря-
         * мого вызова конструктора вызывает метод newInstance(). Активность может пе-
         * редать newInstance(…) любые параметры, необходимые фрагменту для создания
         * аргументов.
         */
        return ItemFragment.newInstance(photo.getId());
    }

    //Возвращает  текущее  количество  элементов  в  списке.
    @Override
    public int getCount() {
        return mListPhotos.size();
    }
}
