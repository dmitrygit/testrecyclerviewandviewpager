package ru.good.di.testrecyclerviewandviewpager.item.viewpager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.List;
import java.util.UUID;

import ru.good.di.testrecyclerviewandviewpager.R;
import ru.good.di.testrecyclerviewandviewpager.item.ItemFragment;
import ru.good.di.testrecyclerviewandviewpager.model.Photo;
import ru.good.di.testrecyclerviewandviewpager.model.PhotoLab;

public class ItemViewPagerActivity extends FragmentActivity {

    public static final String TAG = "ID_PHOTO";

    private static final String EXTRA_ITEM_ID =
            "ru.good.di.testrecyclerviewandviewpager.item_id";

    //Макет состоит из этого экземпляра
    private ViewPager mViewPager;
    private List<Photo> mListPhotos;

    public static Intent newIntent(Context packageContext, UUID photoId){

        Intent intent = new Intent(packageContext, ItemViewPagerActivity.class);

        /**
         * После создания явного интента вызываем putExtra(…), передавая строковый
         * ключ и связанное с ним значение (photoId).вызывается версия :
         * putExtra(String, Serializable)
         */
        intent.putExtra(EXTRA_ITEM_ID, photoId);

        return intent;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Используем layout c ViewPager
         */
        setContentView(R.layout.item_pager_activity);

        UUID photoId = (UUID) getIntent().getSerializableExtra(EXTRA_ITEM_ID);

        mViewPager = (ViewPager) findViewById(R.id.activity_item_pager_view_pager);

        mListPhotos = PhotoLab.get(this).getPhotos();

        FragmentManager fragmentManager = getSupportFragmentManager();

        /**
         * Создаем PagerAdapter
         * Используя FragmentStatePagerAdapter — субкласс PagerAdapter, который берет на себя мно-
         * гие технические подробности.
         * можно не создавать Inner class
         * mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
         * это будет выглядеть следующим образом:
         */
        ItemPagerAdapter pagerAdapter = new ItemPagerAdapter(fragmentManager, mListPhotos);
        mViewPager.setAdapter(pagerAdapter);

        //показывает нужный
        for (int i = 0; i < mListPhotos.size(); i++){
            if (mListPhotos.get(i).getId().equals(photoId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }


    }


}
