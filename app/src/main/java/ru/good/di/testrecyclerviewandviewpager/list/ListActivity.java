package ru.good.di.testrecyclerviewandviewpager.list;

import android.support.v4.app.Fragment;

import ru.good.di.testrecyclerviewandviewpager.SingleFragmentActivity;


public class ListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new ListFragment();
    }

}

