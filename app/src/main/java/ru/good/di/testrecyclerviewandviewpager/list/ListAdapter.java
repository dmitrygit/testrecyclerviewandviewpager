package ru.good.di.testrecyclerviewandviewpager.list;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.good.di.testrecyclerviewandviewpager.R;
import ru.good.di.testrecyclerviewandviewpager.model.Photo;

/**
 *  Класс RecyclerView взаимодействует с адаптером, когда потребуется создать объ-
 * ект ViewHolder или связать его с объектом Photo. Сам виджет RecyclerView ничего
 * не знает об объекте Photo, но адаптер располагает полной информацией о Photo.
 */
public class ListAdapter extends RecyclerView.Adapter<ListViewHolder> {

    public static final String TAG = "ID_PHOTO";
    private List<Photo> mListPhotos;

    public ListAdapter(List<Photo> photos){
        mListPhotos = photos;
    }

    /**
     *      Метод onCreateViewHolder вызывается виджетом RecyclerView, когда ему потре-
     *  буется новое представление для отображения элемента. В этом методе мы создаем
     *  объект View и упаковываем его в ViewHolder. RecyclerView пока не ожидает, что
     *  представление будет связано с какими-либо данными.
     */

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        /**
         *  android.R.layout.simple_list_item_1 - макет содержит один виджет TextView,
         * оформленный так, чтобы он хорошо смотрелся в списке. Но он будет заменен на свой макет.
         */
        View view = layoutInflater
                .inflate(R.layout.list_item, parent, false);

        return new ListViewHolder(view);
    }


    /**
     * Этот метод связывает представление View объекта ViewHolder с объектом модели.
     * При вызове он получает ViewHolder и позицию
     * в наборе данных. Позиция используется для нахождения правильных данных мо-
     * дели, после чего View обновляется в соответствии с этими данными.
     */
    @Override
    public void onBindViewHolder(ListViewHolder holder, final int position) {

        /**
         * В данной реализации эта позиция определяет индекс объекта Photo в массиве.
         */
        Photo mPhoto = mListPhotos.get(position);

        Log.d(TAG, "2: ListAdapter :onBindViewHolder -> " + mPhoto.getName() + " : " + mPhoto.getId());


        /**
         * После получения нужного объекта мы связываем всё с View, присваивая значиния виджетам
         *  из ViewHolder
         */
        //holder.mNameTitleView.setText(mPhoto.getName()); заменил на:
        holder.bindPhoto(mPhoto);
    }
    @Override
    public int getItemCount() {
        return mListPhotos.size();
    }


}