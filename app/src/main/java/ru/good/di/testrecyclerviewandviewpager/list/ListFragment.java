package ru.good.di.testrecyclerviewandviewpager.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.good.di.testrecyclerviewandviewpager.R;
import ru.good.di.testrecyclerviewandviewpager.model.Photo;
import ru.good.di.testrecyclerviewandviewpager.model.PhotoLab;

/**
 * 1. Создание ListFragment
 * 2. Создание ViewHolder
 * 3. Создание Adapter
 */
public class ListFragment extends Fragment{

    public static final String TAG = "ID_PHOTO";

    /**
     * О бязанности RecyclerView сводятся к переработке виджетов
     * TextView и размещению их на экране. Но виджет RecyclerView не занимается разме-
     * щением элементов на экране самостоятельно — он поручает эту задачу LayoutManager.
     * Объект LayoutManager управляет позиционированием элементов, а также опре-
     * деляет поведение прокрутки. Таким образом, при отсутствии LayoutManager виджет
     * RecyclerView просто погибнет в тщетной попытке что-нибудь сделать.
     */
    private RecyclerView mRecyclerView;

    private ListAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container , Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        mRecyclerView = (RecyclerView) view
                .findViewById(R.id.list_recycler_view);

        /**
         *  Сразу же после создания виджета RecyclerView ему назначает-
         * ся другой объект LayoutManager. Это необходимо для работы виджета RecyclerView.
         * Если вы забудете предоставить ему объект LayoutManager, возникнет ошибка.
         * LinearLayoutManager - размещает элементы в вертикальном списке.
         * GridLayoutManager - размещает элементы в виде таблицы.
         */
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //метод updateUI() будет расширяться по мере усложнения
        //пользовательского интерфейса.
        updateUI();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    /**
     * Связываем адаптер(ListAdapter) с RecyclerView.
     */
    private void updateUI(){
        PhotoLab photoLab = PhotoLab.get(getContext());
        List<Photo> photos = photoLab.getPhotos();

        Log.i(TAG, "1: ListFragment: List<Photo> photos -> " + photos.toString());
        if (mAdapter == null) {
            mAdapter = new ListAdapter(photos);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }
}
