package ru.good.di.testrecyclerviewandviewpager.list;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import ru.good.di.testrecyclerviewandviewpager.R;
import ru.good.di.testrecyclerviewandviewpager.item.viewpager.ItemViewPagerActivity;
import ru.good.di.testrecyclerviewandviewpager.model.Photo;

/**
 *  Вместо этого можно обрабатывать касания так, как это обычно делается: назначе-
 * нием обработчика OnClickListener. Так как каждое представление View связыва-
 * ется с некоторым ViewHolder, вы можете сделать объект ViewHolder реализацией
 * OnClickListener для своего View.
 */
public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public static final String TAG = "ID_PHOTO";

    private Photo mPhoto;

    public TextView mNameTitleView;
    public TextView mNameDescriptionView;

    //Конструктор
    public ListViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        //mNameTitleView = (TextView) itemView;
        mNameTitleView = (TextView)
                itemView.findViewById(R.id.list_item_name_text_view);
        mNameDescriptionView = (TextView)
                itemView.findViewById(R.id.list_item_description_text_view);


    }

    //чтобы немного упростить код.
    public void bindPhoto(Photo photo){
        mPhoto = photo;

        mNameTitleView.setText(mPhoto.getName());
        mNameDescriptionView.setText(mPhoto.getDescription());
        //...................
    }

    //обрабатываю клик на item
    @Override
    public void onClick(View v) {
        /*Toast.makeText(v.getContext(),
                mPhoto.getName() + " clicked!", Toast.LENGTH_SHORT)
                .show();*/
        Log.d(TAG, "2: ListAdapter : onClick -> "+ mPhoto.getName() + " : " + mPhoto.getId());

        Intent intent = ItemViewPagerActivity.newIntent(v.getContext(), mPhoto.getId());
        v.getContext().startActivity(intent);

    }
}
