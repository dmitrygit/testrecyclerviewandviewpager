package ru.good.di.testrecyclerviewandviewpager.model;

import java.util.ArrayList;
import java.util.UUID;

public class Photo {
    private UUID mId;
    private String name;
    private String description;

    private ArrayList<String> urlPhoto;

    public Photo() {
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(ArrayList<String> urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    @Override
    public String toString() {
        return "Name:" + name + ". ID:" + mId;
    }
}
