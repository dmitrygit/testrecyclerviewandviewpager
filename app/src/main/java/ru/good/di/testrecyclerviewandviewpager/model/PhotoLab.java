package ru.good.di.testrecyclerviewandviewpager.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class PhotoLab {
    //s статическая переменная
    private static PhotoLab sPhotosLab;


    private List<Photo> mPhotos;

    public static PhotoLab get(Context context){
        if (sPhotosLab == null){
            sPhotosLab = new PhotoLab(context);
        }
        return sPhotosLab;
    }

    private PhotoLab(Context context){
        mPhotos = new ArrayList<>();

        for (int i = 0; i < 20; i++){
            Photo photo = new Photo();

            photo.setName("Раздел: " + i);
            photo.setDescription("Описание.");

            mPhotos.add(photo);
        }

    }

    //Добавление персоны
    public void addPhoto(Photo p){
        mPhotos.add(p);
    }

    public List<Photo>  getPhotos() {
        return mPhotos;
    }

    public Photo getPhoto(UUID id){
        for (Photo photo : mPhotos){
            if (photo.getId().equals(id)){
                return photo;
            }
        }
        return null;
    }
}
